//
//  ViewController.swift
//  TrainingProject
//
//  Created by Rita Kazakova on 1.02.22.
//

import UIKit

class ViewController: UIViewController {
    
    private let logoImageView = UIImageView()
    private let welcomeLabel = UILabel()
    private let arrayOfLanguages = ["English", "Русский", "Беларуская"]
    private let languagePicker: UIPickerView = UIPickerView()
    
    private let darkButton = CustomButton(color: UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1), titleString: NSLocalizedString("dark_button", comment: ""))
    private let lightButton = CustomButton(color: UIColor(red: 0.4, green: 0.4, blue: 0.4, alpha: 1), titleString: NSLocalizedString("light_button", comment: ""))
    private let autoButton = CustomButton(color: UIColor(red: 0.5, green: 0.6, blue: 0.8, alpha: 1), titleString: NSLocalizedString("auto_button", comment: ""))
        
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.init(named: "AccentColor")
        
        languagePicker.delegate = self
        languagePicker.dataSource = self
                
        setupLogoImageView()
        setupWelcomeLable()
        setupButtonsStackView()
        setupLanguagePicker()
        
        darkButton.addTarget(self, action: #selector(darkMode), for: .touchUpInside)
        lightButton.addTarget(self, action: #selector(lightMode), for: .touchUpInside)
        autoButton.addTarget(self, action: #selector(autoMode), for: .touchUpInside)
    }

    private func setupLogoImageView() {
        logoImageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(logoImageView)
        
        logoImageView.image = UIImage(named: "baseline_psychology")
        logoImageView.tintColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1)
        
        logoImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 50).isActive = true
        logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoImageView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 70).isActive = true
        logoImageView.widthAnchor.constraint(equalTo: logoImageView.heightAnchor).isActive = true
    }
    
    private func setupWelcomeLable() {
        welcomeLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(welcomeLabel)

        welcomeLabel.text = NSLocalizedString("label_text", comment: "")
        welcomeLabel.textColor = UIColor.init(named: "TextColor")
        welcomeLabel.font = welcomeLabel.font.withSize(40)
        
        welcomeLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 20).isActive = true
        welcomeLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    private func setupButtonsStackView() {
        let stackViewWithButtons = UIStackView(arrangedSubviews: [darkButton, lightButton, autoButton])

        view.addSubview(stackViewWithButtons)
        stackViewWithButtons.translatesAutoresizingMaskIntoConstraints = false

        stackViewWithButtons.distribution = .fillEqually
        stackViewWithButtons.axis = .horizontal
        stackViewWithButtons.spacing = 10
        
        stackViewWithButtons.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50).isActive = true
        stackViewWithButtons.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        stackViewWithButtons.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
    }
    
    private func setupLanguagePicker() {
        view.addSubview(languagePicker)
        languagePicker.translatesAutoresizingMaskIntoConstraints = false
        
        languagePicker.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        languagePicker.topAnchor.constraint(equalTo: welcomeLabel.bottomAnchor, constant: 40).isActive = true
    }
    
    // MARK: Modes
    @objc func darkMode() {
        overrideUserInterfaceStyle = .dark
        darkButton.setTitleColor(.systemBlue, for: .normal)
        lightButton.setTitleColor(.white, for: .normal)
        autoButton.setTitleColor(.white, for: .normal)
    }
    
    @objc func lightMode() {
        overrideUserInterfaceStyle = .light
        lightButton.setTitleColor(.systemBlue, for: .normal)
        darkButton.setTitleColor(.white, for: .normal)
        autoButton.setTitleColor(.white, for: .normal)
    }
    
    @objc func autoMode() {
        overrideUserInterfaceStyle = .unspecified
        autoButton.setTitleColor(.systemBlue, for: .normal)
        lightButton.setTitleColor(.white, for: .normal)
        darkButton.setTitleColor(.white, for: .normal)
    }
}

extension ViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayOfLanguages.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let row = arrayOfLanguages[row]
        return row
    }
}

extension ViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let categoryName = arrayOfLanguages[row]
        
        if categoryName == arrayOfLanguages[0] {
            welcomeLabel.text = "label_text".localizableString(loc: "en")
            darkButton.setTitle("dark_button".localizableString(loc: "en"), for: .normal)
            lightButton.setTitle("light_button".localizableString(loc: "en"), for: .normal)
            autoButton.setTitle("auto_button".localizableString(loc: "en"), for: .normal)
        } else if categoryName == arrayOfLanguages[1]  {
            welcomeLabel.text = "label_text".localizableString(loc: "ru")
            darkButton.setTitle("dark_button".localizableString(loc: "ru"), for: .normal)
            lightButton.setTitle("light_button".localizableString(loc: "ru"), for: .normal)
            autoButton.setTitle("auto_button".localizableString(loc: "ru"), for: .normal)
        } else if categoryName == arrayOfLanguages[2]  {
            welcomeLabel.text = "label_text".localizableString(loc: "be")
            darkButton.setTitle("dark_button".localizableString(loc: "be"), for: .normal)
            lightButton.setTitle("light_button".localizableString(loc: "be"), for: .normal)
            autoButton.setTitle("auto_button".localizableString(loc: "be"), for: .normal)
        }
    }
}

extension String  {
    func localizableString(loc: String) -> String {
        let path = Bundle.main.path(forResource: loc, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}



//
//  CustomButton.swift
//  TrainingProject
//
//  Created by Rita Kazakova on 3.02.22.
//

import UIKit

class CustomButton: UIButton {
    required init(color: UIColor, titleString: String) {
        super.init(frame: .zero)
        backgroundColor = color
        setTitle(titleString, for: .normal)
        layer.borderWidth = 1
        layer.borderColor = UIColor.black.cgColor
        layer.cornerRadius = 10
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
